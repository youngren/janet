# Janet
![Janet!](https://media.giphy.com/media/xUOxeT5ZpZVStUPxC0/giphy.gif)

## Setup

### Requirements
- Python3.6 
- [Virtualenv](https://virtualenv.pypa.io/en/latest/installation/)

### Install
```
git clone this thing
python3.6 -m virtualenv venv
. venv/bin/activate
pip install -Ur requirements.txt
```

### Set up a new app in Slack

1. Use the [Create App page](https://api.slack.com/apps/new) to create a new bot
2. Add a bot
3. Install the app to your workspace
4. Enable at least the following permissions:
  1. Access user’s public channels (`channels:history`)
  2. Access information about your workspace (`team:read`)
  3. Access your workspace’s profile information (`users:read`)
  4. Access user’s profile and workspace profile fields (`users.profile:read`)
  5. Rename a channel (`channels:writ`e)
5. You'll be prompted to reinstall the app. Do that.
6. Invite your bot to any channels it should be in

### Create an AWS account that has DynamoDB admin privs

I'm not gonna explain how to do this here, because it's very opsy. But, do it.

Then configure your credentials in a boto config file, as described here:

http://boto.cloudhackers.com/en/latest/boto_config_tut.html#credentials

### Configure and run Janet

Export environment variables `JANET_XOXB` and `JANET_XOXP` from your environment. They should contain
the "Bot User OAuth Access Token" and "OAuth Access Token" values, respectively, from the bot's
*OAuth & Permissions* page.

And then run Janet:

```
./run_janet.py
```

## Writing Processor Plugins
If you want to make Janet do something, check out the `BasicProcessors` file in `JanetProcessors`
This has some simple examples you can get started with.

If you really want to get off the ground faster just make a new python module inside of 
the JanetProcessors directory. This module needs a minimum of two things to work.

1. A class that inherits from JanetProcessor
2. A method on that class decorated with @entry_point

like so:
```
from .JanetProcessor import JanetProcessor, entry_point

class MyProcessor(JanetProcessor):
    @entry_point
    def process(self):
    	print("processing an event!")
	do_something_else(self.event)
```

This processor will print `processing an event!` to the console every time your Janet
Receives any event from the Slack RTM API. That's all it takes to get started. The 
event data is stored in Event objects directly on the processor, as `self.event`. 

## Links

### Sentry error logging
@ https://sentry.io/alex-karpinski/janet/

### Gitlab Continuous Deployment
@ https://gitlab.com/alex_karp/janet/pipelines

### Documentation on Read The Docs
@ http://janet.readthedocs.io/

## Notes for devs (me)
for coverage, run tests like:
`coverage run --branch --source . --omit "venv/*" --concurrency gevent run_tests.py`
