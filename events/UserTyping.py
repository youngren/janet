from events.Event import Event
from models.User import User
from datetime import datetime
import colored


class UserTyping(Event):
    expected_type = "user_typing"

    @property
    def sender(self):
        return User.from_source_obj(self)

    def __repr__(self):
        dim = colored.fg("dark_gray")
        reset = colored.attr("reset")
        return f"{self.sender.chroma_hash}{datetime.now().strftime('%H:%M')} - {dim}typing...{reset}"
