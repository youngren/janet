from events.Event import Event


class ReplyableEvent(Event):
    def reply(self, message):
        send_message_args = (message, self.channel_id, self.thread_ts)
        self.send_message(*send_message_args)
        return message

    def reply_dm(self, message):
        self.send_message(message, self.sender.uid)

    # Both this and the next function are the default way Message works
    # At the moment I only have to override it on ReactionAdded
    @property
    def is_in_thread(self):
        thread_ts_exists = "thread_ts" in self.raw_line
        return thread_ts_exists

    @property
    def thread_ts(self):
        if self.is_in_thread:
            return self.raw_line["thread_ts"]
