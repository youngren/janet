import pprint
from datetime import datetime
import time
import colored
from x256 import x256

from events.Event import Event
from events.Message import Message
from events.ReplyableEvent import ReplyableEvent
from models.User import User


class ReactionAdded(ReplyableEvent):
    expected_type = "reaction_added"
    _reacted_to_user = None
    _message = None

    @property
    def sender(self):
        if not self._sender:
            self._sender = User.from_source_object(self, create=True)
        return self._sender

    @property
    def reaction(self):
        return self.raw_line["reaction"]

    @property
    def channel_id(self):
        return self.raw_line["item"]["channel"]

    @property
    def raw_timestamp(self):
        return float(self.raw_line["ts"])

    @property
    def timestamp(self):
        return datetime.fromtimestamp(self.raw_timestamp)

    @property
    def reacted_to_user(self):
        if not self._reacted_to_user:
            self._reacted_to_user = User.from_uid(self.raw_line["item_user"], create=True)
        return self._reacted_to_user

    @property
    def message(self):
        if self._message:
            return self._message

        self._message = Message(channel=self.channel_id, ts=self.raw_line["item"]["ts"])
        return self._message

    @property
    def is_in_thread(self):
        return self.message.is_in_thread

    @property
    def thread_ts(self):
        return self.message.thread_ts

    def __repr__(self):
        return f"{self.sender.chroma_hash}{self.timestamp.strftime('%H:%M')} - {self.sender.display_name}: reacted with :{self.reaction}:"
