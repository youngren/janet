import pprint
from datetime import datetime
import time
import colored
import settings
from x256 import x256

from events.ReplyableEvent import ReplyableEvent
from models.User import User


class Message(ReplyableEvent):
    expected_type = "message"

    def __init__(self, raw_line=None, channel=None, ts=None):
        if channel and ts:
            raw_line = self.channel_and_ts_to_raw_line(channel, ts)

        super().__init__(raw_line)
        if "message" in raw_line:
            self.raw_line = raw_line["message"]
            self.original_line = raw_line  # just in case we want it later, y'know?
        elif "previous_message" in self.raw_line:  # oof idk about this one
            self.raw_line = raw_line["previous_message"]
            self.original_line = raw_line

        if "text" in self.raw_line:
            self.text = self.raw_line["text"]

    @property
    def channel_id(self):
        # This fails for message edited events
        return self.raw_line["channel"]

    @property
    def raw_timestamp(self):
        return float(self.raw_line["ts"])

    @property
    def timestamp(self):
        return datetime.fromtimestamp(self.raw_timestamp)

    @property
    def sender(self):
        if not self._sender:
            self._sender = User.from_source_object(self, create=True)
        return self._sender

    def __repr__(self):
        DEBUG = False
        if DEBUG:
            return pprint.pformat(self.raw_line)
        return f"{self.sender.chroma_hash}{self.timestamp.strftime('%H:%M')} - {self.sender.display_name}: {self.text}"
