from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import i_startswith_words
from itertools import cycle, zip_longest
import random


class Spongecase(JanetProcessor):
    spongecase_words = ("?spongecase",)
    oldsponge_words = ("?oldsponge ", "?randocase ", "?randomcase")

    @i_startswith_words(*spongecase_words)
    def spongecase(self):
        if not self.args:
            return
        plain_text = " ".join(self.args)

        sponger = cycle([str.upper, str.lower])
        spongecased_text = "".join([next(sponger)(letter) for letter in plain_text])
        self.reply(spongecased_text)

        return spongecased_text

    @i_startswith_words(*oldsponge_words)
    def oldsponge(self):
        if not self.args:
            return
        plain_text = " ".join(self.args)

        spongecased_text = "".join(
            [random.choice([letter.upper, letter.lower])() for letter in plain_text]
        )
        self.reply(spongecased_text)
