from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, NumberAttribute, BooleanAttribute
from pynamodb.indexes import GlobalSecondaryIndex, AllProjection
from JanetNeuron import JanetNeuron


class PlusTarget(Model):
    class Meta:
        table_name = f"{JanetNeuron().workspace_name}_pluses"

    target = UnicodeAttribute(hash_key=True)
    plus_count = NumberAttribute(default=200)
    target_is_user = BooleanAttribute(default=False)

    def __repr__(self):
        return f"{self.target}: {self.plus_count}"


if not PlusTarget.exists():
    PlusTarget.create_table(read_capacity_units=1, write_capacity_units=1)
