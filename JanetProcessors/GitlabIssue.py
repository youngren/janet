from JanetProcessors.JanetProcessor import JanetProcessor
from .decorators import entry_point, messages_only, allow_events, startswith_words
from events.Message import Message
import gitlab
import settings


class GitlabIssue(JanetProcessor):
    janet_proj_id = 8963809
    # Check out this high code below
    # I call it a github access token and then use it to access gitlab
    gl = gitlab.Gitlab("https://gitlab.com", private_token=settings.github_access_token)

    @startswith_words("?issue")
    def process(self):
        self.create_issue()

    def create_issue(self):
        issue_title = " ".join(self.args)
        issue_args = {}
        issue_args["title"] = issue_title
        issue_args["description"] = f"requested by {self.event.sender.display_name}"

        new_issue = self.project.issues.create(issue_args)
        self.reply(new_issue.web_url)

    @property
    def project(self):
        return self.gl.projects.get(self.janet_proj_id)

    @property
    def issues(self):
        return self.project.issues.list()
