from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import startswith_words, entry_point
from events.Message import Message
from events.ReactionAdded import ReactionAdded
from .models import EmojiRequest
from models.User import User


class EmojiRequest_Processor(JanetProcessor):
    emoji_request_words = ("?emojirequest ",)
    subcommands = ("list", "done")
    _emoji_name = None

    @startswith_words(*emoji_request_words)
    def process(self):
        if not self.args:
            self.reply(f"{self.random_polite_refusal} or I guess at least give me some arguments")
            return

        cmd = self.args[0]
        if cmd in self.subcommands:
            if cmd == "list":
                return self.list_emoji()
            elif cmd == "done":
                return self.complete_emoji()
        else:
            return self.create_emoji_request()

    def complete_emoji(self):
        try:
            e = EmojiRequest.get(self.emoji_name)
            e.delete()
            self.reply(f"{self.emoji_name} completed")
            return True
        except EmojiRequest.DoesNotExist:
            self.reply(f"{self.random_polite_refusal} I don't even think that request exists")

    def list_emoji(self):
        all_requests = EmojiRequest.scan()
        formatted_requests = []
        for e in all_requests:
            requester = User.from_uid(e.requester_id)
            formatted_request = f"{e.emoji_name} - requested by {requester.at_name}"
            formatted_requests.append(formatted_request)
        self.reply("\n".join(formatted_requests))
        return all_requests

    def create_emoji_request(self):
        try:
            e = EmojiRequest.get(self.emoji_name)
        except EmojiRequest.DoesNotExist:
            e = EmojiRequest(
                self.emoji_name, description=self.description, requester_id=self.event.sender.uid
            )
            e.save()
            self.reply(f"OK, created request for {e.emoji_name}")
        return e

    @property
    def description(self):
        if len(self.args) > 1:
            description = self.args[1:]
        else:
            description = None
        requester_id = self.event.sender.uid

    def slackmojify_word(self, word):
        if not (word.startswith(":")):
            word = ":" + word
        if not (word.endswith(":")):
            word = word + ":"
        return word

    @property
    def emoji_name(self):
        if not self._emoji_name:
            if self.args[0] in self.subcommands:
                emoji_name = self.args[1]
            else:
                emoji_name = self.args[0]
            emoji_name = self.slackmojify_word(emoji_name)
            self._emoji_name = emoji_name
        return self._emoji_name
