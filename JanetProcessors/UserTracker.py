from sqlalchemy.sql.expression import func, select
from sqlalchemy.orm.exc import UnmappedInstanceError
from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import entry_point
from events.Hello import Hello
from events.Message import Message
from events.UserChange import UserChange
import time, math
from models.User import User
import gevent
import sys
from log.metrics import Metric

# idk kinda just seems like the worst code in janet overall
class UserTracker(JanetProcessor):
    sampling_rate = 0.05

    @entry_point()
    def listen(self):
        if isinstance(self.event, Hello) or isinstance(self.event, UserChange):
            self.update_user_tracking()
        elif isinstance(self.event, Message):
            self.process_human()

        if self.should_sample_event:
            self.process_sample()

    def process_human(self):
        if self.event.text.startswith("?name_sync"):
            self.update_user_tracking()
            self.reply("user IDs and names resync'd")
        elif self.event.text.startswith("?name_mappings"):
            self.reply("Okay, here's who I know about right now")
            for user in self.get_all_mappings():
                self.reply(user)
        elif self.event.text.startswith("?who"):
            u = User.from_display_name(" ".join(self.split_text[1:]))

    def get_all_mappings(self):
        all_users = User.scan()
        return all_users

    def process_sample(self):
        if isinstance(self.event, Message) and not self.event.sender.is_bot:
            self.update_by_event()
        else:
            self.update_user_tracking()

    @property
    def should_sample_event(self):
        if self.sampling_rate >= 1:
            return True

        the_time = math.floor((time.time() * 1000))
        the_modulo = 1.0 / self.sampling_rate
        return not bool(the_time % the_modulo)

    def update_user_tracking(self):
        user_list = self.list_users()
        if not user_list:
            self.send_message("{self.random_exclamation}, I'm getting rate limited")

        # fucking bots have a display_name of None and a separate real_name field
        for maybe_bot in user_list:
            # Sometimes fields are on the user directly and sometimes they're on a profile sub-object
            # Fuck that. Merge them together.
            if "profile" in maybe_bot:
                maybe_bot.update(maybe_bot["profile"])

            if "display_name" in maybe_bot and not maybe_bot["display_name"]:
                if "real_name" in maybe_bot:
                    pass

            if (
                "display_name" in maybe_bot
                and "real_name" in maybe_bot
                and not maybe_bot["display_name"]
            ):
                maybe_bot["profile"]["display_name_normalized"] = maybe_bot["real_name"]

            if "id" in maybe_bot and not "uid" in maybe_bot:
                maybe_bot["uid"] = maybe_bot["id"]

        user_mapping = []
        for user in user_list:
            try:
                existing_user = User.get(user["id"])
            except User.DoesNotExist:
                existing_user = None

            display_name = user["profile"]["display_name_normalized"].lower()

            user_mapping.append((user["id"], existing_user, display_name))

        for uid, existing_user, display_name in user_mapping:
            if not existing_user:
                new_user = self.initialize_user(uid)
            elif existing_user.display_name != display_name:
                existing_user.display_name = display_name
                existing_user.save()

        Metric("names_synced", value=1).put()

    def initialize_user(self, uid):
        user = User.from_uid(uid)

        if not user:
            display_name = self.userid_to_display_name(uid).lower()
            new_user = User(uid=uid, display_name=display_name)
            new_user.save()
            user = new_user

        return user

    def update_by_event(self):
        try:
            user = User.get(self.event.sender.uid)
        except User.DoesNotExist:
            user = self.initialize_user(self.event.sender.uid)

        if user.display_name != self.event.sender.display_name:
            user.display_name = self.event.sender.display_name.lower()
            user.save()
