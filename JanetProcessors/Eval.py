from .JanetProcessor import JanetProcessor
from .decorators import i_startswith_words

# imports by request of users
import random, json


class Eval(JanetProcessor):
    eval_words = ("?python", "?eval", "?exec")

    @i_startswith_words(*eval_words)
    def process(self):
        user_input = " ".join(self.args)
        eval_output = eval(user_input)
        self.reply(eval_output)
