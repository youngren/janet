import os

slack_tokens = {}
slack_tokens["bot"] = os.getenv("JANET_XOXB")
slack_tokens["user"] = os.getenv("JANET_XOXP")
github_access_token = os.getenv("JANET_GHAT")
db_connect_string = "sqlite:///db.sqlite"
worker_count = 5

# You need some delay so that the greenlets have a chance to process
mainloop_delay = 0.05

SENTRY_ENV = os.getenv("SENTRY_ENV")
# You should be fine if you just set your SENTRY_DSN env var
