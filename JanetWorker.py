import gevent
import inspect
import pprint
import time
import sentry_sdk
import zmq.green as zmq

from JanetNeuron import JanetNeuron
import JanetProcessors
from events.cast_table import cast_table
from events.GenericEvent import GenericEvent
from log.metrics import Metric
import settings

# JanetWorkers are persistent workers with all the functionality
# of a neuron plus dedicated functionality for processing events.
# By default all queue processing janets are basic JanetWorkers
class JanetWorker(JanetNeuron):
    _event = None

    def __init__(self, zmq_context):
        sentry_sdk.init(environment=settings.SENTRY_ENV, release=self.version)
        self._processors = []
        self.zmq_context = zmq_context

    # Worker Janets are callable. Functions are first-class citizens after all!
    # Each worker neuron is initialized with a queue and calling the instance e.g. janet_worker()
    # throws it into an infinte loop where it grabs and processes events as fast as possible
    def __call__(self):
        try:
            req_sock = self.zmq_context.socket(zmq.PULL)
            req_sock.connect("ipc:///tmp/janet_q_out")
            while True:
                # raw_line is where everything else comes from.
                # If you change the raw_line you change the state of the entire worker.
                self.raw_line = req_sock.recv_json()

                begin_time = time.time()
                self.process_event()
                end_time = time.time()

                # Just metrics
                event_processing_time = end_time - begin_time
                Metric("event_processing_time", value=event_processing_time).put()
        except Exception as e:
            sentry_sdk.capture_exception(e)
            Metric("worker_exception", value=1).put()
        finally:
            self.send_message("worker dying")
            req_sock.close()
            gevent.sleep(1)
            self()

    # keep 'em freeeeesssshhh
    @property
    def processors(self):
        if not self._processors:
            for processor_class in JanetProcessors:
                processor = processor_class(self.event)
                self._processors.append(processor)
        else:
            for processor in self._processors:
                processor.event = self.event
        return self._processors

    # there's a little magic in here
    # do not look directly at the janet
    def process_event(self):
        print(self.event)  # This is where we print the event log to the console
        for processor in self.processors:
            for entry_point in processor.entry_points:
                # you have to call the processing function with the instance as the first
                # argument because it's become an unbound function. We're just manually adding in `self`
                entry_point(processor)

    @property
    def event(self):
        if self._event and self.raw_line == self._event.raw_line:
            pass
        else:
            type_name = self.raw_line["type"]
            if type_name in cast_table:
                self._event = cast_table[type_name](self.raw_line)
            else:
                self._event = GenericEvent(self.raw_line)

        return self._event
