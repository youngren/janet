from slackclient import SlackClient
from settings import slack_tokens, session_id
import hashlib

from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, JSONAttribute

# This class holds data about the Slack app itself
# This helps avoid hitting the API all the time
# It also re-implements features found in Neuron. This is because
# Janet Neuron needs easy access to session data and this prevents an import loop
# help me
class Session(Model):
    class Meta:
        table_name = "janet_sessions"

    session_id = UnicodeAttribute(hash_key=True)
    message_response = JSONAttribute()
    team_info = JSONAttribute()
    user_info = JSONAttribute()
    bot_info = JSONAttribute()

    _client = None

    def sync_up(self, *args, **kwargs):
        self.message_response = self.ugh_fine("message_response")
        self.user_info = self.ugh_fine("user_info")
        self.bot_info = self.ugh_fine("bot_info")
        self.team_info = self.ugh_fine("team_info")

        return self

    # I swear I will never understand this after tonight
    # this is such a mistake
    def ugh_fine(self, attr_name):
        missing = False
        if hasattr(self, attr_name) and not getattr(self, attr_name):
            missing = True
        elif hasattr(self, attr_name) and getattr(self, attr_name):
            if "ok" in getattr(self, attr_name) and getattr(self, attr_name)["ok"]:
                missing = False  # for human readability here
            else:
                missing = True

        if missing:
            print(f"generating data for {attr_name}...")
            func_name = f"make_{attr_name}"
            if hasattr(self, func_name) and callable(getattr(self, func_name)):
                return getattr(self, func_name)()
            else:
                raise ValueError(f"no matching setter for {attr_name}")
        else:
            return getattr(self, attr_name)

    def make_message_response(self):
        client = SlackClient(slack_tokens["bot"])
        response = client.api_call(
            "chat.postMessage", channel="@slackbot", as_user=True, text="who am i"
        )
        return response

    def make_bot_info(self):
        client = SlackClient(slack_tokens["bot"])
        response = client.api_call("bots.info", bot=self.get_bot_id())
        return response

    def make_team_info(self):
        client = SlackClient(slack_tokens["user"])
        response = client.api_call("team.info")
        return response

    def make_user_info(self):
        client = SlackClient(slack_tokens["user"])
        response = client.api_call("team.info")
        return response

    def get_bot_id(self):
        return self.message_response["message"]["bot_id"]


if not Session.exists():
    Session.create_table(read_capacity_units=10, write_capacity_units=10)
