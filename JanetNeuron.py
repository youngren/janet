import pprint
import importlib
import subprocess
import settings
import gevent
import random
from slackclient import SlackClient
from models.Session import Session


class JanetNeuron:
    _session = (
        None
    )  # kinda seems like session should be taking care of its own lazy loading, right?

    @property
    def tokens(self):
        # disable reloading settings to get new tokens
        # this was nice for development but unnecessary overhead now
        # importlib.reload(settings)
        return settings.slack_tokens

    @property
    def bot_token(self):
        return self.tokens["bot"]

    @property
    def user_token(self):
        return self.tokens["user"]

    @property
    def bot_client(self):
        return SlackClient(self.bot_token)

    client = bot_client

    @property
    def user_client(self):
        return SlackClient(self.user_token)

    def leave_channel(self, channel_id):
        response = self.client.api_call("channels.leave", channel=channel_id)
        return response

    def send_message(self, message, channel="#the-bad-place", thread_ts=None):
        response = self.client.api_call(
            "chat.postMessage",
            channel=channel,
            thread_ts=thread_ts,
            as_user=True,
            text=message,
        )
        return response

    def send_image_from_file(self, filename, channel="#the-good-place"):
        sc.api_call("chat.postMessage", channel=channel, attachments=attachments)
        return response

    def user_info_from_id(self, userid):
        response = self.client.api_call("users.info", user=userid)
        if not "user" in response:
            return None

        return response["user"]

    # You probably don't want this
    # you can get a Message straight from channel and ts via its init
    def channel_and_ts_to_raw_line(self, channel, ts):
        response = self.user_client.api_call(
            "channels.history", channel=channel, latest=ts, inclusive=True, count=1
        )
        return response["messages"][0]

    def userid_to_display_name(self, uid):
        user_info = self.user_info_from_id(uid)
        if user_info["profile"]["display_name_normalized"]:
            return user_info["profile"]["display_name_normalized"].lower()
        else:
            return user_info["profile"]["real_name_normalized"].lower()

    def display_name_to_userid(self, display_name):
        user_info = self.user_info_from_id(uid)
        return user_info["profile"]["display_name_normalized"].lower()

    def list_users(self):
        for i in range(5):
            response = self.client.api_call("users.list")
            if response["ok"]:
                return response["members"]
            else:
                gevent.sleep(2 ** i)
        raise

    def rename_channel(self, channel_id, new_name):
        response = self.user_client.api_call(
            "channels.rename", channel=channel_id, name=new_name
        )
        return response

    @property
    def conversations(self):
        response = self.client.api_call("conversations.list")
        if response["ok"]:
            return response["channels"]

    @property
    def channels(self):
        return [c for c in self.conversations if c["is_channel"]]

    @property
    def all_emoji(self):
        response = self.user_client.api_call("emoji.list")
        emoji = response["emoji"]
        return emoji

    @property
    def random_cactus(self):
        return ":cactus{}:".format(random.randint(2, 15))

    @property
    def random_polite_refusal(self):
        phrases = [
            "No, sorry",
            "Oof, I already have plans",
            ":confused: maybe next time",
            "lol wut?",
        ]
        random.shuffle(phrases)
        return phrases[0]

    @property
    def random_exclamation(self):
        phrases = ["Y-uh oh"]
        random.shuffle(phrases)
        return phrases[0]

    @property
    def session(self):
        if not self._session:
            try:
                self._session = Session.get(settings.session_id)
            except Session.DoesNotExist:
                self._session = Session(settings.session_id)
                self._session.sync_up()
                self._session.save()
        return self._session

    @property
    def version(self):
        git_vers = subprocess.check_output(
            ["git", "rev-parse", "--short", "HEAD"]
        ).strip()
        if not git_vers:
            return "unknown version"
        else:
            return git_vers.decode("ascii")

    @property
    def team_info(self):
        return self.session.team_info["team"]

    @property
    def workspace_name(self):
        return self.team_info["name"]

    @property
    def bot_id(self):
        return self.bot_info["id"]

    @property
    def bot_info(self):
        return self.session.bot_info["bot"]

    @property
    def name(self):
        return self.bot_info["name"]


class StaticJanetNeuron(JanetNeuron):
    _client = None

    @property
    def client(self):
        if not self._client:
            self._client = SlackClient(self.bot_token)
        return self._client


class SilentJanetMixin:
    def send_message(self, message, channel, thread_ts):
        pass


class SilentJanetNeuron(JanetNeuron, SilentJanetMixin):
    pass
