import boto3
import datetime
import time
import random

from JanetNeuron import JanetNeuron


class Metric(JanetNeuron):
    def __init__(self, metric_name, value):
        self.metric_name = metric_name
        self.value = value
        self.timestamp = datetime.datetime.utcnow()

    def put(self):
        self.metric_namespace = f"{self.workspace_name}:{self.name}"
        cloudwatch = boto3.resource("cloudwatch")
        metric = cloudwatch.Metric(self.metric_namespace, self.metric_name)
        metric.put_data(
            MetricData=[
                {
                    "MetricName": metric.metric_name,
                    "Timestamp": self.timestamp,
                    "Value": self.value,
                    "Unit": "None",
                }
            ]
        )
